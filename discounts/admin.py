from django.contrib import admin

from .models import Discount

class DiscountAdminSite(admin.ModelAdmin):
    list_display  = ('value', )
    search_fields = ['value', ]

admin.site.register(Discount, DiscountAdminSite)
