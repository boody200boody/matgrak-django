from django.db import models

class Discount(models.Model):
    class Meta:
        verbose_name = 'Discount'
        verbose_name_plural = 'Discounts'
    value      = models.FloatField(blank=False, null=False, default=0)