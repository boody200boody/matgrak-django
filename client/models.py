from django.db import models
from plan.models import Plan
from home.models import Home
from django.contrib.auth.models import User

class Client(models.Model):
    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'
    user_id      = models.ForeignKey(User, on_delete=models.CASCADE)
    plan_id      = models.ForeignKey(Plan, on_delete=models.CASCADE)
    shop_id      = models.ForeignKey(Home, on_delete=models.CASCADE)
    subscription = models.DateTimeField(null=True, blank=True, auto_now_add=False)
    created_at 	 = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)
    updated_at   = models.DateTimeField(auto_now_add=False, auto_now=True, blank=True)

