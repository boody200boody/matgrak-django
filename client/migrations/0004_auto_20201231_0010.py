# Generated by Django 2.2 on 2020-12-30 22:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0003_auto_20201231_0008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='user_id',
            field=models.CharField(max_length=255),
        ),
    ]
