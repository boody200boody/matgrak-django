# Generated by Django 2.2 on 2020-12-30 22:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_auto_20201231_0000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='user_id',
            field=models.PositiveIntegerField(),
        ),
    ]
