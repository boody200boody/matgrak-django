import re

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _, ngettext
import gzip
from pathlib import Path
# User._meta.get_field('email')._unique = True



class SignUpForm(UserCreationForm):


    def numeric_validate(value):
        if value.isdigit():
            raise ValidationError(
                _("هذا الباسورد ضعيف جدا"),
                code='password_entirely_numeric',
            )

    def alpha_validator(value):
        if not re.compile(r'^[a-zA-Z]+$').match(value):
            raise ValidationError(_('من فضلك ادخل حروف فقط'))

    first_name = forms.CharField(
        label='',
        max_length=30,
        # min_length=5,
        required=True,
        widget=forms.TextInput(
            attrs={
                "placeholder": _("الاسم الاول"),
                "class": "form-control"
            }
        ),
        error_messages={'required': _('من فضلك ادخل الاسم الاول')},
        validators=[alpha_validator]
    )

    last_name = forms.CharField(
        label='',
        max_length=30,
        # min_length=5,
        required=True,
        widget=forms.TextInput(
            attrs={
                "placeholder": _("اسم العائلة"),
                "class": "form-control"
            }
        ),
        error_messages={'required': _('من فضلك ادخل اسم العائلة')},
        validators=[alpha_validator]
    )

    email = forms.EmailField(
        label='',
        max_length=254,
        widget=forms.EmailInput(
            attrs={
                "placeholder": _("البريد الالكتروني"),
                "class": "form-control"
            }
        ),
        error_messages={'required': _('من فضلك ادخل البريد الالكتروني'), 'unique': _('البريد الالكتروني مستخدم بالفعل')}
    )

    username = forms.CharField(
        label='',
        max_length=30,
        # min_length=5,
        required=False,
        widget=forms.TextInput(
            attrs={
                "placeholder": _("أسم المستخدم"),
                "class": "form-control"
            }
        ),
        error_messages={'unique': _('البريد الالكتروني مستخدم بالفعل')}

    )

    password1 = forms.CharField(
        label='',
        max_length=30,
        # min_length=8,
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": _("الباسورد"),
                "class": "form-control"
            }
        ),
        error_messages={'required': _('من فضلك ادخل الباسورد'), 'password_too_common': _('هذا الباسورد شائع جدا'),
                        'password_entirely_numeric': _('هذا الباسورد شائع جدا'), 'password_too_short': _('هذا الباسورد ضعيف جدا'),
                        'password_too_similar': 'ss'},
        validators=[numeric_validate]
    )

    password2 = forms.CharField(
        label='',
        max_length=30,
        min_length=8,
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": _("تأكيد الباسورد"),
                "class": "form-control"
            }
        ),
            error_messages={'required': _('من فضلك ادخل تأكيد الباسورد'), 'password_too_common': _('هذا الباسورد شائع جدا'),
                            'min_length': _('ادخل باسورد لا يقل عن 8 احرف'), 'max_length': _('ادخل باسورد لا يزيد عن 30 احرف')},
        validators=[numeric_validate]
    )

    class Meta:
        model = User
        model.username = model.email
        USERNAME_FIELD = 'email'
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')