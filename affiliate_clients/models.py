from django.db import models
from django.contrib.auth.models import User

from client.models import Client
from home.models import Home


class AffilateClients(models.Model):
    class Meta:
        verbose_name = 'Affilate Client'
        verbose_name_plural = 'Affilate Clients'
    client_id    = models.ForeignKey(Client, on_delete=models.CASCADE)
    shop_id      = models.ForeignKey(Home, on_delete=models.CASCADE)
    owner_id     = models.ForeignKey(User, on_delete=models.CASCADE)