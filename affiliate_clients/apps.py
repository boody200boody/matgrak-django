from django.apps import AppConfig


class AffiliateClientsConfig(AppConfig):
    name = 'affiliate_clients'
