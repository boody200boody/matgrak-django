# Generated by Django 2.2 on 2021-01-28 21:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('client', '0006_auto_20210128_1544'),
        ('home', '0025_auto_20210127_2225'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AffilateClients',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='client.Client')),
                ('owner_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('shop_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.Home')),
            ],
            options={
                'verbose_name': 'Affilate Client',
                'verbose_name_plural': 'Affilate Clients',
            },
        ),
    ]
