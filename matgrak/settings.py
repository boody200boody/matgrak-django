import os
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'l%22ak@qpd7!@$*760f8_1)t)usntc=ngyhind0d_!*mef&z1z'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

GEOIP_PATH = os.path.dirname(__file__)


# Application definition

INSTALLED_APPS = [
    'jazzmin',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third Party Apps
    'django.contrib.sites',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.google',
    'paypal.standard.ipn',
    'phonenumber_field',

    # Pro Apps
    'home',
    'client',
    'plan',
    'pay_logs',
    'users',
    'affiliate_market',
    'discounts',
    'affiliate_clients',

    'django.contrib.sitemaps',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'geoip2_extras.middleware.GeoIP2Middleware',

    'django.middleware.locale.LocaleMiddleware',
]

ROOT_URLCONF = 'matgrak.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                # `allauth` needs this from django
                'django.template.context_processors.request',
            ],
        },
    },
]

WSGI_APPLICATION = 'matgrak.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        # 'NAME': 'matgrak',
        # 'USER': 'matgrak',
        # 'PASSWORD': 'pMUpi8fv1jvuD+vf',
        # 'HOST': 'localhost',
        'NAME': 'matgrak',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT':'3306',

        "OPTIONS": {
                    "unix_socket": "/opt/lampp/var/mysql/mysql.sock",
                }
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },

]

SITE_ID = 1


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'ar'

LOCALE_PATHS = (
    os.path.join(os.path.dirname(__file__), "locale"),
)

TIME_ZONE = 'Africa/Cairo'

USE_I18N = True

USE_L10N = True

USE_TZ = True

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    # 'allauth.account.auth_backends.AuthenticationBackend',
]

# ACCOUNT_DEFAULT_HTTP_PROTOCOL='https'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR), "static")
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_in_env", "static_root")
# STATIC_ROOT = os.path.join(BASE_DIR, 'static/')


MEDIA_ROOT= os.path.join(BASE_DIR, 'media/')
MEDIA_URL= "/media/"

PAYPAL_RECEIVER_EMAIL = 'oasis.dev.apps@gmail.com'
PAYPAL_TEST = True

ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'






# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
# ACCOUNT_DEFAULT_HTTP_PROTOCOL='https'
# SECURE_SSL_REDIRECT = True


LANGUAGES = [
    ('ar', _('Arabic')),
    ('en', _('English')),
    ('de', _('Deutsch')),
]


LOGIN_REDIRECT_URL = "/"
ACCOUNT_LOGOUT_REDIRECT_URL ="/"

LOGIN_URL = 'login'
LOGOUT_URL = 'logout'


# ACCOUNT_DEFAULT_HTTP_PROTOCOL='https'
# # Force https redirect
# SECURE_SSL_REDIRECT = True
# # Honor the 'X-Forwarded-Proto' header for request.is_secure()
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
#
#
# SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
# ACCOUNT_DEFAULT_HTTP_PROTOCOL='https'
# SECURE_SSL_REDIRECT = True




