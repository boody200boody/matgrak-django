from django.db import models
from client.models import Client
from plan.models import Plan
# Create your models here.

class PayLog(models.Model):
    client_id    = models.IntegerField(blank=False, null=False)
    plan_id      = models.ForeignKey(Plan, on_delete=models.CASCADE)
    created_at 	= models.DateTimeField(auto_now_add=True, auto_now=False, blank=True, null=True)
    updated_at   = models.DateTimeField(auto_now_add=False, auto_now=True, blank=True, null=True)