from django.apps import AppConfig


class PayLogsConfig(AppConfig):
    name = 'pay_logs'
