$(document).ready(function(){
    /*$('.custom-message').fadeIn(1000).fadeOut(8000);

    $(".js-example-tags").select2({
      tags: true
    });*/

    var alert_message   = $('.custom-message');
    var alert_type      = alert_message.data('value');
    var alert_content   = alert_message.text();

    if( alert_type == 'success' )
    {
        toastr.success(alert_content);
    }
    if( alert_type == 'warning' )
    {
        toastr.warning(alert_content);
    }
    if( alert_type == 'error' )
    {
        toastr.error(alert_content);
    }

    $('#example').countdown({
    date: '9/9/2021 23:59:59'
    }, function () {
//      alert('Merry Christmas!');
    });

    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    dots:false,
    nav:true,
    mouseDrag:false,
    autoplay:false,
    animateOut: 'slideOutUp',
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        10000:{
            items:1
        }
    }
});

$('.navbar-toggle').click(function(e){
    e.preventDefault();
});

$('.mouse-down').click(function(){
    $('#navigation').slideUp();
});

$('.navigation-menu li').click(function(){

    $('.navigation-menu li').removeClass('active');
    $(this).toggleClass('active');
});

});