from django.db import models
from django.contrib.auth.models import User
from home.models import Home


class Affilate(models.Model):
    class Meta:
        verbose_name = 'Affilate Market'
        verbose_name_plural = 'Affilate Markets'
    user_id      = models.ForeignKey(User, on_delete=models.CASCADE)
    code         = models.PositiveIntegerField(blank=False, null=False, default=0)