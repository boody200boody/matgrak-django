from django.apps import AppConfig


class AffiliateMarketConfig(AppConfig):
    name = 'affiliate_market'
