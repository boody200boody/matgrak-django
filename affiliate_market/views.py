from django.contrib.auth import get_user_model
from django.http import HttpResponse, request
from django.shortcuts import render, redirect
from .models import Affilate
import random
import string
from random import randint


def program(request, code):
    original_code = str(code[5:11])
    return HttpResponse(original_code)


def generate_code(size=6, chars= string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def create_affiliate(userModel):
    # if not request.user.is_authenticated:
    #     return redirect('home')

    # if request.method == "POST":
    affiliate = Affilate()

    affiliate.code = generate_code(6)
    affiliate.user_id = userModel

    affiliate_count = affiliate.objects.filter(code=generate_code).count()
    if affiliate_count > 0:
        affiliate.code = affiliate.code.replace(affiliate.code[randint(0, 9)], generate_code(1), 1)

    affiliate.save()