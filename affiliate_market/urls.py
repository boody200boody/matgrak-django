from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from . import views
from django.conf import settings
from home.sitemaps import StaticViewSitemap
from django.contrib.sitemaps.views import sitemap
# sitemaps = {
#     'static': StaticViewSitemap
# }

urlpatterns = [

    # path(r'^program/(?P<code>\w{0,50})/$', views.program, name='program'),
    url(r'^/(?P<code>\d{1,50})/$', views.program, name='program')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)