# Generated by Django 2.2 on 2021-01-07 15:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0023_auto_20210107_1707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='home',
            name='password_hash',
            field=models.CharField(blank=True, default=models.CharField(max_length=50), max_length=50, null=True),
        ),
    ]
