# Generated by Django 2.2 on 2020-12-14 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20201214_2327'),
    ]

    operations = [
        migrations.AlterField(
            model_name='home',
            name='domain',
            field=models.CharField(max_length=15),
        ),
    ]
