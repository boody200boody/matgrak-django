# Generated by Django 2.2 on 2020-12-21 22:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_home_activity'),
    ]

    operations = [
        migrations.AddField(
            model_name='home',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='home',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
