from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.

class Home(models.Model):
    class Meta:
        verbose_name = 'Shop'
        verbose_name_plural = 'Shops'
    domain              = models.CharField(max_length=15, null=False, blank=False, unique=True)
    firstname           = models.CharField(max_length=50, null=False, blank=False)
    lastname            = models.CharField(max_length=50, null=False, blank=False)
    timezone            = models.CharField(max_length=150, null=False, blank=False)
    country             = models.CharField(max_length=150, null=False, blank=True)
    phone               = models.CharField(null=False, blank=False, max_length=20)
    activity            = models.IntegerField(null=False, blank=True, default=0)
    email               = models.EmailField(max_length=50, null=False, blank=False, unique=True)
    password            = models.CharField(max_length=50, null=False, blank=False)
    password_hash       = models.CharField(max_length=50, null=True, blank=True, default=password)
    status              = models.IntegerField(null=False, blank=True, default=0)
    created_at 	        = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True, null=True)
    updated_at          = models.DateTimeField(auto_now_add=False, auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.domain