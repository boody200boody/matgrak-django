from django.contrib import admin

from .models import Home

class ShopAdminSite(admin.ModelAdmin):
    list_display  = ('domain','firstname', 'email')
    search_fields = ['domain','firstname', 'email']

admin.site.register(Home,ShopAdminSite)
