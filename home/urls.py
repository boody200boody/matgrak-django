from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from home import views
from django.conf import settings
from home.sitemaps import StaticViewSitemap
from django.contrib.sitemaps.views import sitemap
# sitemaps = {
#     'static': StaticViewSitemap
# }

urlpatterns = [

    path('admin', admin.site.urls),
    path('', views.index, name='home'),
    path('handler404', views.handler404, name='handler404'),
    path('wait/<str:shop>', views.page_wait, name='page_wait'),
    path('prices', views.prices, name='prices'),

    path('shop_status', views.shop_status, name='shop_status'),

    path('process-payment/', views.process_payment, name='process_payment'),
    path('payment-cancelled/', views.payment_canceled, name='payment_cancelled'),
    path('payment_done', views.payment_done, name='payment_done'),
    path('create-shop', views.create, name='create_shop'),
    path('about/', views.about, name='about'),
    path('create_auth', views.create_auth, name='create_auth'),
    path('signup', views.signup, name='signup'),

    path('profile', views.profile, name='profile'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)