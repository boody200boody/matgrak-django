from datetime import datetime
import os
import subprocess
from django.utils import timezone
from django.utils import timezone

from django.dispatch import receiver
from django.shortcuts import render, redirect
from paypal.standard.models import ST_PP_COMPLETED

from affiliate_market.views import create_affiliate
from users.forms import SignUpForm
from .forms import HomeForm
from django.contrib import messages as message, messages
from subprocess import Popen
from django.conf import settings
from django.urls import reverse
from django.shortcuts import render, get_object_or_404
from paypal.standard.forms import PayPalPaymentsForm
from django.views.decorators.csrf import csrf_exempt
import json
from django.contrib.staticfiles.templatetags.staticfiles import static
from ip2geotools.databases.noncommercial import DbIpCity
import pycountry
import pytz
import allauth
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

from client.models import Client
from plan.models import Plan
from allauth.socialaccount.models import SocialAccount
from .models import Home
from django.http import JsonResponse
from django.http import HttpResponse
from paypal.standard.ipn.signals import valid_ipn_received
import hashlib
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from django.contrib.auth.models import User
from django.contrib import auth
from django.shortcuts import render_to_response

from django.contrib.gis.geoip2 import GeoIP2
from django.utils.translation import gettext as _


def index(request):
    context = {}
    planes = Plan.objects.all()
    context['planes'] = planes
    signup_form = SignUpForm(request.POST)
    context['signup_form'] = signup_form

    # output = subprocess.check_output(['matgrk-generate', 'shop7 uo2@uo.com Uo123456 eg Europe/Paris'])
    # print(output)

    if request.user.is_authenticated:
        userModel = get_user_model().objects.get(username=request.user)
        print(userModel.email)
        shop_count = Home.objects.filter(email=userModel.email).count()
        print(shop_count)
        context['shop_count'] = shop_count
        if shop_count > 0:
            shop = Home.objects.get(email=userModel.email)
            print(shop.domain)
            # admin_panel = subprocess.check_output(['matgrk-admin-path', shop.domain])
            admin_panel = 'https://demo.matgrk.shop'
            context['admin_panel'] = admin_panel


    return render(request, 'home/index.html', context)

def handler404(request, exception, template_name="home/page-404.html"):
    response = render_to_response(template_name)
    response.status_code = 404
    return response

def prices(request):
    context = {}
    planes = Plan.objects.all()
    context['planes'] = planes

    if request.user.is_authenticated:
        userModel = get_user_model().objects.get(username=request.user)
        print(userModel.email)
        shop_count = Home.objects.filter(email=userModel.email).count()
        print(shop_count)
        context['shop_count'] = shop_count
        if shop_count > 0:
            shop = Home.objects.get(email=userModel.email)
            print(shop.domain)
            # admin_panel = subprocess.check_output(['matgrk-admin-path', shop.domain])
            admin_panel = 'https://demo.matgrk.shop'
            context['admin_panel'] = admin_panel

    return render(request, 'home/prices.html', context)


def page_wait(request, *args, **kwargs):
    context = {}
    # name = 'Abdelrhman'
    # domain = domain
    # email = email
    # password = password
    # country_code = country_code
    # timezone = timezone
    # context = {'domain': domain, 'email': email, 'password': password, 'country_code': country_code, 'timezone': timezone, 'name': name}
    # print("wait")
    # print(domain)
    # print(email)
    # print(password)
    # command = domain + ' ' + email + ' ' + password + ' ' + country_code + ' ' + timezone
    # output = subprocess.check_output(['matgrk-generate', 'shop13 uo2@uo.com Uo123456 eg Europe/Paris'])
    # print(output)
    shop = kwargs.get('shop')
    context['shop'] =shop
    return render(request, 'home/wait.html', context)


def create(request):
    context = {}
    print(hashlib.md5(str(123456).encode()).hexdigest())
    if not request.user.is_authenticated:
        return redirect('home')

    if request.user.is_authenticated:
        userModel = get_user_model().objects.get(username=request.user)
        print(userModel.email)
        shop_count = Home.objects.filter(email=userModel.email).count()
        print(shop_count)
        context['shop_count'] = shop_count
        if shop_count > 0:
            shop = Home.objects.get(email=userModel.email)
            print(shop.domain)
            # admin_panel = subprocess.check_output(['matgrk-admin-path', shop.domain])
            admin_panel = 'https://demo.matgrk.shop'
            context['admin_panel'] = admin_panel
            messages.warning(request, _('لقد قمت بتسجيل متجر الكتروني من قبل'))
            return redirect('home')

    # social_info = request.user.socialaccount_set.all()[0].extra_data
    # context['social_info'] = social_info

    userModel = get_user_model().objects.get(username=request.user)

    if request.method == 'POST':
        form = HomeForm(request.POST)
        context['form'] = form
        context['old_values'] = request.POST
        if form.is_valid():
            try:
                shop = Home()
                shop.domain             = form.cleaned_data.get('domain').lower()
                # shop.firstname        = form.cleaned_data.get('firstname').lower()
                # shop.lastname         = form.cleaned_data.get('lastname').lower()
                shop.firstname          = userModel.first_name
                shop.lastname           = userModel.last_name
                shop.password           = form.cleaned_data.get('password')
                # shop.password_hash    = hashlib.md5(form.cleaned_data.get('password')).hexdigest()
                shop.password_hash      = form.cleaned_data.get('password')
                # shop.email            = form.cleaned_data.get('email').lower()
                shop.email              = userModel.email
                shop.country            = form.cleaned_data.get('country').lower()
                shop.timezone           = form.cleaned_data.get('timezone')
                shop.phone              = form.cleaned_data.get('phone')
                # shop.activity         = form.cleaned_data.get('activity')
                shop.activity           = 0
                shop.status             = 0
                shop.save()

                # message.success(request, 'Congratulation you have successfully your shop creation')
                saved_shop = Home.objects.get(domain=shop.domain)
                print(shop.pk)
                print(saved_shop.id)
                print(form.cleaned_data.get('domain'))
                print(form.cleaned_data.get('country'))
                print(form.cleaned_data.get('timezone'))
                print(form.cleaned_data.get('email'))

                client = Client()
                # client.user_id = social_info['id']
                # client.name = social_info['name']
                client.user_id  = userModel.pk
                client.name     = form.cleaned_data.get('name')
                client.subscription = timezone.now()
                client.plan_id_id = 1
                client.shop_id_id = shop.pk
                client.save()

                domain              = form.cleaned_data.get('domain')
                email               = form.cleaned_data.get('email')
                password            = form.cleaned_data.get('password')
                country_code        = form.cleaned_data.get('country')
                timezones           = form.cleaned_data.get('timezone')

                context['domain']       = domain
                context['email']        = email
                context['password']     = password
                context['country_code'] = country_code
                context['timezone']     = timezone.now()

                context['userModel'] = userModel

                messages.success(request, _('لقد قمت بأدخال بيانات صحيحة\n جاري انشاء متجرك '))
                return redirect('page_wait', domain)
            except Exception as e:
                print(e)
                messages.warning(request, _('من فضلك قم بتصحيح الاخطاء التالية '))
                #return redirect('create_shop', context)
        else:
            # form = HomeForm()
            temp = form.cleaned_data
            print("Djando Error")
            print(temp)
            messages.warning(request, _('عفوا. بيانات غير صحيحة'))

    client_ip = request.META['REMOTE_ADDR']
    ip_country = DbIpCity.get('156.215.188.233', api_key='free').country
    user_country = pycountry.countries.get(alpha_2=ip_country)

    context['client_ip']    = client_ip
    context['user_country'] = user_country
    context['timezones']    = pytz.all_timezones
    context['countries2']   = pycountry.countries
    context['userModel']    = userModel



    # output = subprocess.check_output(['matgrk-generate', 'shop7 uo2@uo.com Uo123456 eg Europe/Paris'])
    # print(output)

    return render(request, 'home/create_shop.html', context)

def process_payment(request):
    if not request.user.is_authenticated:
        return redirect('home')

    userModel = get_user_model().objects.get(username=request.user)

    if request.method == 'POST':
        host = request.get_host()
        if request.POST['plan_name'].lower() == 'trial':
            return redirect('home')
        plan = Plan.objects.get(name=request.POST['plan_name'].lower())
        print(plan.name)
        print(plan.size)
        print(plan.price)
        print(plan.plugins)
        paypal_dict = {
            'business': settings.PAYPAL_RECEIVER_EMAIL,
            'amount': str(plan.price),
            'item_name': str(plan.name) + ' Plane',
            'invoice': 'Matgrak Payment Invoice',
            'currency_code': 'USD',
            'custom': 'abdelrhman paid',
            'notify_url': 'https://{}{}'.format(host,
                                               reverse('paypal-ipn')),
            'return_url': 'https://{}{}'.format(host,
                                               reverse('payment-done')),
            'cancel_return': 'https://{}{}'.format(host,
                                                  reverse('payment_cancelled')),
        }

        form = PayPalPaymentsForm(initial=paypal_dict)
        return render(request, 'home/process_payment.html', {'form': form, 'plan': plan})
    else:
        return redirect('home')

def shop_status(request):
    # domain = request.GET.get('domain')
    userModel = get_user_model().objects.get(username=request.user)
    shop = Home.objects.get(email=userModel.email)
    shop = Home.objects.get(domain=shop.domain)
    print(shop)
    return JsonResponse({'status': shop.status}, safe=False)

@csrf_exempt
def payment_done(request):
    context = {}
    # if not request.user.is_authenticated:
    #     return redirect('home')

    # social_info = request.user.socialaccount_set.all()[0].extra_data
    # context['social_info'] = social_info

    return render(request, 'home/payment_done.html', context)


@csrf_exempt
def payment_canceled(request):
    return render(request, 'home/payment_cancelled.html')


def create_auth(request):
    if request.POST:
        email   = request.POST['email']
        password = request.POST['password']

        user_count = get_user_model().objects.filter(email=email).count()
        if user_count <= 0:
            return redirect('home')

        UserModel = get_user_model().objects.get(email=email)
        username = UserModel.username

        print(username, password)
        user = authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            Status = [{"validation": "Login Successful", "status": True}]
            message.success(request, _('تسجيل دخول ناجح'))
            return redirect('home')
            # return HttpResponse(json.dumps(Status), content_type="application/json")

        else:
            Status = [{"validation": "Authentication failure", "status": False}]
            # return HttpResponse(json.dumps(Status), content_type="application/json")
            message.error(request, _('خطأ في البريد الالكتروني او كلمة السر'))
            return redirect('home')
    else:
        # return HttpResponse(json.dumps([{"validation": "I am watching you (0_0)", "status": False}]), content_type="application/json")
        return redirect('home')

def signup(request):
    if request.user.is_authenticated:
        return redirect('home')

    context = {}
    if request.method == "POST":
        form = SignUpForm(request.POST)
        context['form'] = form
        context['old_values'] = request.POST
        # form = SignUpForm()
        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.username = form.cleaned_data.get('email')
            new_form.save()

            username = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)

            userModel = get_user_model().objects.get(username=request.user)
            # create_affiliate(userModel)


            messages.success(request, _('مرحبا بك في متجرك\n لقد قمت بتسجيل مستخدم بناجح '))
            return redirect('create_shop')
        else:
            messages.warning(request, _('من فضلك قم بتصحيح الاخطاء التالية'))
    else:
        form = SignUpForm()
        context['form'] = form

    return render(request, 'home/signup.html', context)



def profile(request):
    if not request.user.is_authenticated:
        return redirect('home')

    context = {}
    userModel = get_user_model().objects.get(username=request.user)
    shop_count = Home.objects.filter(email=userModel.email).count()

    if shop_count <= 0:
        return redirect('home')

    shop = Home.objects.get(email=userModel.email)
    client = Client.objects.get(user_id=userModel.id)

    # print(os.listdir('/home/abdelrhman/Django/matgrak-django/'))
    path = '/home/abdelrhman/Django/matgrak-django/'
    dirs = os.listdir(path)
    admin_panel = ''
    for dir in dirs:
        # if os.path.isdir(dir) and not str(dir).startswith(".") and str(dir).startswith('admin'):
        if os.path.isdir(dir) and 'admin' in dir:
            admin_panel = dir
            context['admin_panel'] = admin_panel

    if admin_panel == '' and shop != None:
        route = '/wait/' + shop.domain
        return redirect(route)

    context['shop_count'] = shop_count
    context['userModel'] = userModel
    context['client'] = client
    context['shop'] = shop

    return render(request, 'home/profile.html', context)


def about(request):
    return HttpResponse('about page')


