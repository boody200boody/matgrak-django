from django.shortcuts import get_object_or_404
from client.models import Client
from paypal.standard.ipn.signals import valid_ipn_received
from django.dispatch import receiver


@receiver(valid_ipn_received)
def payment_notification(sender, **kwargs):
    ipn = sender
    if ipn.payment_status == 'Completed':
        # payment was successful
        client = get_object_or_404(Client, id=ipn.invoice)
        print('Completed Payment')

        if client.total_cost() == ipn.mc_gross:
            # mark the order as paid
            client.plan_id = 2
            client.save()