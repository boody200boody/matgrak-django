import re

from django import forms
from django.core.exceptions import ValidationError
from phonenumber_field.modelfields import PhoneNumberField

from .models import Home
from django.utils.translation import gettext as _

class HomeForm(forms.ModelForm):

    def domain_validator(value):
        if not re.compile(r'^[ a-zA-Z]+-?[a-zA-Z0-9]+$').match(value):
            raise ValidationError(_('من فضلك ادخل ( Domain ) حروف فقط مثال shop '))
        if " " in value:
            raise ValidationError(_('من فضلك ادخل ( Domain ) لا يحتوي على مسافات') )

        blocked_domains = ['matgrk', 'matgrak', 'mtgrk', 'google', 'gmail', 'facebook','twitter', 'instgram','youtube', 'demo', 'example', 'www', 'domain',
                           'http', 'https', ]

        for block in blocked_domains:
            if str(value).lower() == block:
                raise ValidationError(_('هذا ال ( Domain ) مستخدم بالفعل'))

    def password_lengh_validation(value):
        if len(value) < 6:
            raise ValidationError(_('من فضلك ادخل باسورد قوي'))
        if " " in value:
            raise ValidationError(_('من فضلك ادخل ( باسورد ) لا يحتوي على مسافات'))

    def phone_validator(value):
        if len(value) < 4 or len(value) > 20:
            raise ValidationError(_('من فضلك ادخل ( رقم هاتف ) صحيح'))

        if not re.compile(r'^\d{4,20}$').match(value):
            raise ValidationError(_('من فضلك ادخل ( رقم هاتف ) صحيح يتكون من ارقام فقط '))


    domain = forms.CharField(
      widget=forms.TextInput(attrs={
        'autocomplete': 'off',
        'required': 'required'
      }),
      error_messages={'required': _('من فضلك ادخل ال ( Domain )'), 'unique': _("هذا ال ( Domain ) مستخدم بالفعل"), 'invalid': _("من فضلك ادخل ( Domain ) صحيح ")},
      validators=[domain_validator]
    )

    firstname = forms.CharField(
      widget=forms.TextInput(attrs={
        'autocomplete': 'off',
        'required': 'required'
      }),
      error_messages={'required': _('من فضلك ادخل الاسم الاول')}
    )

    lastname = forms.CharField(
      widget=forms.TextInput(attrs={
        'autocomplete': 'off',
        'required': 'required'
      }),
      error_messages={'required': _('منفضلك ادخل اسم العائلة')}
    )

    timezone = forms.CharField(
      widget=forms.TextInput(attrs={
        'autocomplete': 'off',
        'required': 'required'
      }),
      error_messages={'required': _('من فضلك ادخل المنطقة الزمنية')}
    )

    country = forms.CharField(
      widget=forms.TextInput(attrs={
        'autocomplete': 'off',
        'required': 'required'
      }),
      error_messages={'required': _('من فضلك ادخل البلد')}
    )

    phone = forms.CharField(
        widget=forms.TextInput(attrs={
            'autocomplete': 'off',
            'required': 'required'
        }),
        error_messages={'required': _('من فضلك ادخل رقم الهاتف')},
        validators=[phone_validator]
    )

    activity = forms.CharField(
        widget=forms.NumberInput(attrs={
            'autocomplete': 'off',
            # 'required': 'required'
        }),
        error_messages={'required': _('من فضلك ادخل النشاط')}
    )

    email = forms.CharField(
        widget=forms.EmailInput(attrs={
            'autocomplete': 'off',
            # 'required': 'required'
        }),
        error_messages={'required': _('من فضلك ادخل البريد الالكتروني'), 'unique': _("هذا ( البريد الالكتروني ) مستخدم بالفعل")}
    )

    password = forms.CharField(
      widget=forms.TextInput(attrs={
        'autocomplete': 'off',
        # 'required': 'required'
      }),
      error_messages={'required': _('من فضلك ادخل الباسورد')},
        validators=[password_lengh_validation]
    )

    # password_hash = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Home
        fields = '__all__'