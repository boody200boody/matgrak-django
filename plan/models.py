from django.db import models

# Create your models here.
class Plan(models.Model):
    class Meta:
        verbose_name = 'Plan'
        verbose_name_plural = 'Plans'
    name        = models.CharField(max_length=100, blank=True, null=False)
    number      = models.SmallIntegerField(blank=False, null=False, default=-1)
    size        = models.IntegerField(blank=False, null=False)
    plugins     = models.CharField(max_length=255, blank=False, null=False)
    price       = models.FloatField(blank=False, null=False)
    created_at 	= models.DateTimeField(auto_now_add=True, auto_now=False, blank=True, null=True)
    updated_at  = models.DateTimeField(auto_now_add=False, auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name