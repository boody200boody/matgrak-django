from django.contrib import admin

from .models import Plan

class PlanAdminSite(admin.ModelAdmin):
    list_display = ('name','price', 'size', 'number', 'plugins')
    search_fields = ['name','price', 'size', 'plugins']

admin.site.register(Plan,PlanAdminSite)